from keras import backend as K
import tensorflow as tf

def lovasz_jac_loss():
    """
    A function to compute the Lovasz-Softmax Loss
    for higher Jaccard similarity in segmentation test_masks
    """
    def loss(y_true, y_pred):
        # scale preditctions so that class probabilities of each sample
        # sum to 1
        y_pred /= K.sum(y_pred, axis=-1, keepdims=True)
        # clip to avoid NaN and inf classes
        y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())
        # calculate the Lovasz-softmax loss help_function
        loss = lovasz_softmax_flat(y_true, y_pred, classes='present')
        return loss
    return loss

def lovasz_softmax_flat(y_true, y_pred, classes):
    """
    Lovasz Softmax Loss on flattened tensors
    """
    C = 2
    losses = []
    present = []
    class_to_sum = list(range(C)) if classes in ['all', 'present'] else classes
    for c in class_to_sum:
        fg = tf.cast(tf.equal(y_true, c), y_pred.dtype)  # foreground for class c
        if classes == 'present':
            present.append(tf.reduce_sum(fg) > 0)
        errors = tf.abs(fg - y_pred[:, c])
        errors_sorted, perm = tf.nn.top_k(errors, k=tf.shape(errors)[0], name="descending_sort_{}".format(c))
        fg_sorted = tf.gather(fg, perm)
        gradient = lovasz_grad(fg_sorted)
        losses.append(tf.tensordot(errors_sorted, tf.stop_gradient(gradient), 1, name="loss_class_{}".format(c)))

    if len(class_to_sum) == 1:  # short-circuit mean when only one class
        return losses[0]
    losses_tensor = tf.stack(losses)
    if classes == 'present':
        present = tf.stack(present)
        losses_tensor = tf.boolean_mask(losses_tensor, present)
    loss = tf.reduce_mean(losses_tensor)
    return loss

def lovasz_grad(gt_sorted):
    """
    Computes gradient of the Lovasz extension w.r.t sorted errors
    See Alg. 1 in paper
    """
    gts = tf.reduce_sum(gt_sorted)
    intersection = gts - tf.cumsum(gt_sorted)
    union = gts + tf.cumsum(1. - gt_sorted)
    jaccard = 1. - intersection / union
    jaccard = tf.concat((jaccard[0:1], jaccard[1:] - jaccard[:-1]), 0)
    return jaccard
