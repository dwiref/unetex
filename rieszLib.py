import numpy as np
import cv2
from skimage import exposure


def structureTensor(I, gx, gy):
	assert len(I.shape) == 2
	sigma = 0.7
	m, n = I.shape
	dx = np.empty((m,n))
	dy = np.empty((m,n))
	dxy = np.empty((m,n))
	dx = cv2.GaussianBlur(gx*gx, (5, 5), sigma)
	dy = cv2.GaussianBlur(gy*gy, (5, 5), sigma)
	dxy = cv2.GaussianBlur(gx*gy, (5, 5), sigma)

	return dx, dxy, dy

def evals(I, j1, j2, j3, j4):
	assert len(I.shape) == 2
	m, n = I.shape
	e1 = np.empty((m,n))
	e2 = np.empty((m,n))
	for i in range(0, m):
		for j in range(0, n):
			ST = np.array([[j1[i,j], j2[i,j]], [j3[i,j], j4[i,j]]])
			e, v = np.linalg.eig(ST)
			e1[i,j] = e[0]
			e2[i,j] = e[1]
	return e1, e2

def rieszft(image):
	assert len(image.shape) == 2
	m, n = image.shape
	m_low = -np.floor(m/2)
	m_high = np.ceil((m/2))
	n_low  = -np.floor(n/2)
	n_high = np.ceil((n/2))
	w1, w2 = np.mgrid[m_low : m_high, n_low : n_high]
	RZ = (np.exp(1j*np.arctan2(-w2, w1)))
	rxf = -1j*np.real(RZ)
	ryf = -1j*np.imag(RZ)
	rxf = np.fft.ifftshift(rxf)
	ryf = np.fft.ifftshift(ryf)
	SP = np.fft.fft2(image)
	riesz_x = np.fft.ifft2(SP*rxf)
	riesz_y = np.fft.ifft2(SP*ryf)
	rx = np.real(riesz_x)
	ry = np.real(riesz_y)
	return rx, ry

def stvals(image, j1, j2, j3, j4, e1, e2):
	assert len(image.shape)==2
	m, n = image.shape
	energy = np.empty((m,n))
	coherence = np.empty((m,n))
	orientation = np.empty((m,n))

	emax = 0
	epsilon = 0.00000001
	energy = np.abs(e1) + np.abs(e2)
	emax = np.amax(energy)
	coherence = np.sqrt((j4-j1)*(j4-j1) + 4*(j2*j2))/(j4+j1+epsilon)
	orientation = 0.5*np.arctan((2*j2)/(np.add(np.subtract(j4,j1),epsilon)))

	return energy, coherence, orientation, emax

def normalize(p, nval):
	assert len(p.shape) == 2
	m, n = p.shape
	e = np.empty((m,n))
	e = 255*p/nval

	return e

def imNormalize(array, minVal, maxVal):
	m, n = array.shape
	outArray = np.zeros((m,n))
	outArray = ((maxVal - minVal)*np.subtract(array, np.amin(array))/(np.subtract(np.amax(array), np.amin(array)))) + minVal
	
	return outArray

def vesselRiesz(image):
	#clahe = cv2.createCLAHE(clipLimit=15.0, tileGridSize=(5,5))
	#image = clahe.apply(image)
	#image = cv2.GaussianBlur(image, (9,9), 0.3)
	image = exposure.equalize_adapthist(image, 8, clip_limit=0.05)
	#image = imNormalize(image, 0, 1)
	assert len(image.shape) == 2
	M, N = image.shape
	rx, ry = rieszft(image)
	j1, j2, j4 = structureTensor(image, rx, ry)
	j3 = np.copy(j2)
	e1, e2 = evals(image, j1, j2, j3, j4)
	energy, coherence, orientation, emax = stvals(image, j1, j2, j3, j4, e1, e2)
	energy = np.asarray(imNormalize(energy, 0.0, 1.0), np.float64)
	orientation_x = orientation*180/np.pi
	orientation_2 = orientation_x + 90
	coherence = np.asarray(imNormalize(coherence, 0.0, 1.0), np.float64)
	orientation_2 = np.asarray(imNormalize(orientation, 0, 1), np.float64)
	return image, energy, coherence, orientation_2

