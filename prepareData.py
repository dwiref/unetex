import os
from multiprocessing import Pool
import h5py
import cv2
import pickle
from rieszLib import *
from PIL import Image
import multiprocessing as mp
import matplotlib.pyplot as plt

runtype = "Messidor"
setNo = "set4"
imgPath = "./"+runtype+"/"+setNo+"/images/"
maskPath = "./"+runtype+"/"+setNo+"/masks/"
#gndPath = "./"+runtype+"/ground/"+setNo+"/"
outPath = "./dme/"
n_imgs = 9
height = 744
width = 1120

#def get_datasets(groundTruth_dir,borderMasks_dir):
def get_datasets(borderMasks_dir):
#groundTruth = np.empty((n_imgs,height,width))
    border_masks = np.empty((n_imgs,height,width))
            #corresponding border masks
    for path, subdirs, files in os.walk(borderMasks_dir):
        files = sorted(files)
        for i in range(len(files)):
            border_masks_name = files[i]
            print ("border masks name: " + border_masks_name)
            b_mask = cv2.imread(borderMasks_dir + border_masks_name)[:,:,0]
            b_mask = cv2.resize(b_mask, (1120, 744), interpolation=cv2.INTER_AREA)
            border_masks[i] = np.asarray(b_mask)

    print(np.max(border_masks))
    print(np.min(border_masks))
    assert(np.max(border_masks)==255)
    assert(np.min(border_masks)==0)
    print ("border masks are correctly withih pixel value range 0-255 (black-white)")
    #assert(imgs.shape == (n_imgs,height,width))
    border_masks = np.reshape(border_masks,(n_imgs,1,height,width))
    #groundTruth = np.reshape(groundTruth, (n_imgs, 1, height, width))
    assert(border_masks.shape == (n_imgs,1,height,width))
    #return groundTruth, border_masks
    return border_masks

def write_hdf5(arr,outfile):
  with h5py.File(outfile,"w") as f:
    f.create_dataset("image", data=arr, dtype=arr.dtype)

masks = get_datasets(borderMasks_dir=maskPath)

write_hdf5(masks, (outPath+"border_masks_"+runtype+"_"+setNo+".hdf5"))
#write_hdf5(gnd, (outPath+"groundTruth_"+runtype+"_"+setNo+".hdf5"))

data = np.empty((3, height, width))
print("Beginning image preprocessing...")

for path, subdirs, files in os.walk(imgPath): #list all files, directories in the path
    files = sorted(files)
print(range(len(files)))
def doData(i):
    imgPath = "./"+runtype+"/"+setNo+"/images/"
    print ("original image: " +files[i])
    img = cv2.imread(imgPath+files[i])
    img = cv2.resize(img, (1120, 744), interpolation=cv2.INTER_AREA)
    print(img.shape)
    imG = np.zeros((height, width))
    print(imG.shape)
    imG = img[:,:,1]
    print("Computing Structure Tensor metrics for image: " +files[i])
    im, e, c, o = vesselRiesz(imG)
    imG = imNormalize(imG, 0.0, 1.0)
    data[0, :, :] = imG
    data[1, :, :] = e
    data[2, :, :] = c
    #cv2.imshow("0", im)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    # data[2, :, :] = o
    return data


with Pool(processes=20) as pool:
    data = pool.map(doData, range(len(files)))  # process data_inputs iterable with pool
    pool.close()
    pool.terminate()
    pool.join()
    print(len(data))

data = np.reshape(np.asarray(data), (n_imgs, 3, height, width))

write_hdf5(data, (outPath+runtype+"_"+setNo+"_data.hdf5"))
print("Preprocessing complete.")
print("Pushing data to h5py file...")
print("Done! Saved as "+runtype+"_"+setNo+"_data.hdf5")
print("Complete.")
